from collections import defaultdict
from typing import List, Optional, Tuple

import cv2
import numpy as np
import warnings
from ..data.filter import filter_tl_faces_by_status
from ..data.map_api import MapAPI
from ..geometry import rotation33_as_yaw, transform_point, transform_points, world_to_image_pixels_matrix,yaw_as_rotation33
from .rasterizer import Rasterizer
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# sub-pixel drawing precision constants
CV2_SHIFT = 8  # how many bits to shift in drawing
CV2_SHIFT_VALUE = 2 ** CV2_SHIFT


def get_ego_as_agent(frame: np.ndarray) -> np.ndarray:  # TODO this can be useful to have around
    """
    Get a valid agent with information from the frame AV. Ford Fusion extent is used

    Args:
        frame (np.ndarray): the frame we're interested in

    Returns: an agent np.ndarray of the AV

    """
    ego_agent = np.zeros(1, dtype=AGENT_DTYPE)
    ego_agent[0]["centroid"] = frame["ego_translation"][:2]
    ego_agent[0]["yaw"] = rotation33_as_yaw(frame["ego_rotation"])
    ego_agent[0]["extent"] = np.asarray((EGO_EXTENT_LENGTH, EGO_EXTENT_WIDTH, EGO_EXTENT_HEIGHT))
    return ego_agent


def draw_boxes(
    raster_size: Tuple[int, int],
    world_to_image_space: np.ndarray,
    agents: np.ndarray,
    im
) -> np.ndarray:
    """
    Draw multiple boxes in one sweep over the image.
    Boxes corners are extracted from agents, and the coordinates are projected in the image plane.
    Finally, cv2 draws the boxes.

    Args:
        raster_size (Tuple[int, int]): Desired output image size
        world_to_image_space (np.ndarray): 3x3 matrix to convert from world to image coordinated
        agents (np.ndarray): array of agents to be drawn
        color (Union[int, Tuple[int, int, int]]): single int or RGB color

    Returns:
        np.ndarray: the image with agents rendered. RGB if color RGB, otherwise GRAY
    """

    print(len(agents))
    box_world_coords = np.zeros((len(agents), 4, 2))
    corners_base_coords = np.asarray([[-1, -1], [-1, 1], [1, 1], [1, -1]])

    # compute the corner in world-space (start in origin, rotate and then translate)
    for idx, agent in enumerate(agents):
        corners = corners_base_coords * agent["extent"][:2] / 2  # corners in zero
        r_m = yaw_as_rotation33(agent["yaw"])
        box_world_coords[idx] = transform_points(corners, r_m) + agent["centroid"][:2]

    box_image_coords = transform_points(box_world_coords.reshape((-1, 2)), world_to_image_space)

    # fillPoly wants polys in a sequence with points inside as (x,y)
    box_image_coords = box_image_coords.reshape((-1, 4, 2)).astype(np.int64)
    cv2.fillPoly(im, box_image_coords, color=255)
#     return im

def elements_within_bounds(center: np.ndarray, bounds: np.ndarray, half_extent: float) -> np.ndarray:
    """
    Get indices of elements for which the bounding box described by bounds intersects the one defined around
    center (square with side 2*half_side)

    Args:
        center (float): XY of the center
        bounds (np.ndarray): array of shape Nx2x2 [[x_min,y_min],[x_max, y_max]]
        half_extent (float): half the side of the bounding box centered around center

    Returns:
        np.ndarray: indices of elements inside radius from center
    """
    x_center, y_center = center

    x_min_in = x_center > bounds[:, 0, 0] - half_extent
    y_min_in = y_center > bounds[:, 0, 1] - half_extent
    x_max_in = x_center < bounds[:, 1, 0] + half_extent
    y_max_in = y_center < bounds[:, 1, 1] + half_extent
    return np.nonzero(x_min_in & y_min_in & x_max_in & y_max_in)[0]


def cv2_subpixel(coords: np.ndarray) -> np.ndarray:
    """
    Cast coordinates to numpy.int but keep fractional part by previously multiplying by 2**CV2_SHIFT
    cv2 calls will use shift to restore original values with higher precision

    Args:
        coords (np.ndarray): XY coords as float

    Returns:
        np.ndarray: XY coords as int for cv2 shift draw
    """
    coords = coords * CV2_SHIFT_VALUE
    coords = coords.astype(np.int)
    return coords



class MySemanticRasterizer(Rasterizer):
    """
    Rasteriser for the vectorised semantic map (generally loaded from json files).
    """

    def __init__(
        self,
        raster_size: Tuple[int, int],
        pixel_size: np.ndarray,
        ego_center: np.ndarray,
        semantic_map_path: str,
        world_to_ecef: np.ndarray,
        filter_agents_threshold,
        history_num_frames
    ):
        self.raster_size = raster_size
        self.pixel_size = pixel_size
        self.ego_center = ego_center

        self.world_to_ecef = world_to_ecef

        self.proto_API = MapAPI(semantic_map_path, world_to_ecef)

        self.bounds_info = self.get_bounds()

    # TODO is this the right place for this function?
    def get_bounds(self) -> dict:
        """
        For each elements of interest returns bounds [[min_x, min_y],[max_x, max_y]] and proto ids
        Coords are computed by the MapAPI and, as such, are in the world ref system.

        Returns:
            dict: keys are classes of elements, values are dict with `bounds` and `ids` keys
        """
        lanes_ids = []
        crosswalks_ids = []

        lanes_bounds = np.empty((0, 2, 2), dtype=np.float)  # [(X_MIN, Y_MIN), (X_MAX, Y_MAX)]
        crosswalks_bounds = np.empty((0, 2, 2), dtype=np.float)  # [(X_MIN, Y_MIN), (X_MAX, Y_MAX)]

        for element in self.proto_API:
            element_id = MapAPI.id_as_str(element.id)

            if self.proto_API.is_lane(element):
                lane = self.proto_API.get_lane_coords(element_id)
                x_min = min(np.min(lane["xyz_left"][:, 0]), np.min(lane["xyz_right"][:, 0]))
                y_min = min(np.min(lane["xyz_left"][:, 1]), np.min(lane["xyz_right"][:, 1]))
                x_max = max(np.max(lane["xyz_left"][:, 0]), np.max(lane["xyz_right"][:, 0]))
                y_max = max(np.max(lane["xyz_left"][:, 1]), np.max(lane["xyz_right"][:, 1]))

                lanes_bounds = np.append(lanes_bounds, np.asarray([[[x_min, y_min], [x_max, y_max]]]), axis=0)
                lanes_ids.append(element_id)

            if self.proto_API.is_crosswalk(element):
                crosswalk = self.proto_API.get_crosswalk_coords(element_id)
                x_min = np.min(crosswalk["xyz"][:, 0])
                y_min = np.min(crosswalk["xyz"][:, 1])
                x_max = np.max(crosswalk["xyz"][:, 0])
                y_max = np.max(crosswalk["xyz"][:, 1])

                crosswalks_bounds = np.append(
                    crosswalks_bounds, np.asarray([[[x_min, y_min], [x_max, y_max]]]), axis=0,
                )
                crosswalks_ids.append(element_id)

        return {
            "lanes": {"bounds": lanes_bounds, "ids": lanes_ids},
            "crosswalks": {"bounds": crosswalks_bounds, "ids": crosswalks_ids},
        }
    
    def check_for_shared_point(self,sleft,sright,left,right,centroid,agent_yaw):
        spoints=[]
        points=[]
        spoints+=[sleft[0],sleft[-1],sright[0],sright[-1]]
        points+=[left[0],left[-1],right[0],right[-1]]
        
        for spnt in spoints:
            spnt_normal = transform_point(spnt-centroid[:2],  yaw_as_rotation33(-agent_yaw))
            
            if spnt_normal[0]>=0:
                for pnt in points:
                    if np.sum(abs(spnt-pnt))<2:
                        return True
        return False
    
    def find_adjacent_lanes(self,center_world,world_to_image_space, tl_faces,ego_translation,img,raster_radius,active_tl_ids,lanes_lines,
                            left_funcs,right_funcs,lane_indicies,selected_indicies,centroid,agent_yaw):
        
        for in_idx in selected_indicies:
            selected_lane_coords = self.proto_API.get_lane_coords(self.bounds_info["lanes"]["ids"][in_idx])
            selected_xy_left = cv2_subpixel(transform_points(selected_lane_coords["xyz_left"][:, :2], world_to_image_space))
            selected_xy_right = cv2_subpixel(transform_points(selected_lane_coords["xyz_right"][:, :2], world_to_image_space))
            for i in range(len(lane_indicies)):

                idx=lane_indicies[i]
                if idx==in_idx:continue
                lane = self.proto_API[self.bounds_info["lanes"]["ids"][idx]].element.lane

                # get image coords
                lane_coords = self.proto_API.get_lane_coords(self.bounds_info["lanes"]["ids"][idx])
                xy_left = cv2_subpixel(transform_points(lane_coords["xyz_left"][:, :2], world_to_image_space))
                xy_right = cv2_subpixel(transform_points(lane_coords["xyz_right"][:, :2], world_to_image_space))
                lanes_area = np.vstack((xy_left, np.flip(xy_right, 0)))  # start->end left then end->start right

                if self.check_for_shared_point(selected_lane_coords["xyz_left"][:, :2],
                                               selected_lane_coords["xyz_right"][:, :2],
                                               lane_coords["xyz_left"][:, :2],
                                               lane_coords["xyz_right"][:, :2],centroid,agent_yaw):
                    
                    cv2.fillPoly(img, [lanes_area], (17, 17, 31), lineType=cv2.LINE_AA, shift=CV2_SHIFT)

                    lane_type = "default"  # no traffic light face is controlling this lane
                    lane_tl_ids = set([MapAPI.id_as_str(la_tc) for la_tc in lane.traffic_controls])
                    for tl_id in lane_tl_ids.intersection(active_tl_ids):
                        if self.proto_API.is_traffic_face_colour(tl_id, "red"):
                            lane_type = "red"
                        elif self.proto_API.is_traffic_face_colour(tl_id, "green"):
                            lane_type = "green"
                        elif self.proto_API.is_traffic_face_colour(tl_id, "yellow"):
                            lane_type = "yellow"

                    lanes_lines[lane_type].extend([xy_left, xy_right])


    
    def find_lanes_of_points(self,center_world,world_to_image_space, tl_faces,ego_translation,img,raster_radius,active_tl_ids,lanes_lines,
                            left_funcs,right_funcs,lane_indicies):
        selected_indicies=[]
        for i in range(len(lane_indicies)):
                idx=lane_indicies[i]
                lane = self.proto_API[self.bounds_info["lanes"]["ids"][idx]].element.lane

                # get image coords
                lane_coords = self.proto_API.get_lane_coords(self.bounds_info["lanes"]["ids"][idx])
                xy_left = cv2_subpixel(transform_points(lane_coords["xyz_left"][:, :2], world_to_image_space))
                xy_right = cv2_subpixel(transform_points(lane_coords["xyz_right"][:, :2], world_to_image_space))
                lanes_area = np.vstack((xy_left, np.flip(xy_right, 0)))  # start->end left then end->start right
                left_func=left_funcs[i]
                right_func=right_funcs[i]
                
                boundry=self.bounds_info["lanes"]["bounds"][idx]
    #             observation=self.coords_future[45]
                observation=ego_translation
        
                if (left_func(observation[0])-observation[1])*(right_func(observation[0])-
                        observation[1])<=0 and \
                        boundry[0][0]<=observation[0]<=boundry[1][0] and \
                        boundry[0][1]<=observation[1]<=boundry[1][1]:         
                    selected_indicies.append(idx)
                    cv2.fillPoly(img, [lanes_area], (17, 17, 31), lineType=cv2.LINE_AA, shift=CV2_SHIFT)

                    lane_type = "default"  # no traffic light face is controlling this lane
                    lane_tl_ids = set([MapAPI.id_as_str(la_tc) for la_tc in lane.traffic_controls])
                    for tl_id in lane_tl_ids.intersection(active_tl_ids):
                        if self.proto_API.is_traffic_face_colour(tl_id, "red"):
                            lane_type = "red"
                        elif self.proto_API.is_traffic_face_colour(tl_id, "green"):
                            lane_type = "green"
                        elif self.proto_API.is_traffic_face_colour(tl_id, "yellow"):
                            lane_type = "yellow"
                    
                    lanes_lines[lane_type].extend([xy_left, xy_right])

        return selected_indicies
    def rasterize(
        self,
        history_frames: np.ndarray,
        history_agents: List[np.ndarray],
        history_tl_faces: List[np.ndarray],
        agent: Optional[np.ndarray] = None,
    ) -> np.ndarray:
        # TODO TR_FACES
            
        
        
        if agent is None:
            ego_translation = history_frames[0]["ego_translation"]
            ego_yaw = rotation33_as_yaw(history_frames[0]["ego_rotation"])
            ego_agent = get_ego_as_agent(history_frames[0]).astype(agent.dtype)

        else:
            ego_translation = np.append(agent["centroid"], history_frames[0]["ego_translation"][-1])
            ego_yaw = agent["yaw"]
            ego_agent = agent
            

        world_to_image_space = world_to_image_pixels_matrix(
            self.raster_size,
            self.pixel_size,
            ego_translation,
            ego_yaw, 
            self.ego_center,
        )

        # get XY of center pixel in world coordinates
        center_pixel = np.asarray(self.raster_size) * (0.5, 0.5)
        center_world = transform_point(center_pixel, np.linalg.inv(world_to_image_space))

        
        sem_im = self.render_semantic_map(center_world, world_to_image_space, history_tl_faces[0],ego_translation,ego_yaw,ego_agent)
        return sem_im.astype(np.float32) / 255

    
    
    def render_semantic_map(
        self, center_world: np.ndarray, world_to_image_space: np.ndarray, tl_faces: np.ndarray,ego_translation,ego_yaw,ego_agent
    ) -> np.ndarray:
        """Renders the semantic map at given x,y coordinates.

        Args:
            center_world (np.ndarray): XY of the image center in world ref system
            world_to_image_space (np.ndarray):
        Returns:
            np.ndarray: RGB raster

        """

        
        img = 255 * np.ones(shape=(self.raster_size[1], self.raster_size[0], 3), dtype=np.uint8)
#         draw_boxes(self.raster_size, world_to_image_space, np.array([ego_agent]),img)
#         img=img.astype(np.float32) / 255
        # filter using half a radius from the center
        raster_radius = float(np.linalg.norm(self.raster_size * self.pixel_size)) / 2

        # get active traffic light faces
        active_tl_ids = set(filter_tl_faces_by_status(tl_faces, "ACTIVE")["face_id"].tolist())
        # plot lanes
        lanes_lines = defaultdict(list)
        left_funcs=[]
        right_funcs=[]   
        lane_indicies=[]  
        
        for idx in elements_within_bounds(center_world, self.bounds_info["lanes"]["bounds"], raster_radius):
                lane = self.proto_API[self.bounds_info["lanes"]["ids"][idx]].element.lane
                # get image coords
                lane_coords = self.proto_API.get_lane_coords(self.bounds_info["lanes"]["ids"][idx])
                xy_left = cv2_subpixel(transform_points(lane_coords["xyz_left"][:, :2], world_to_image_space))
                xy_right = cv2_subpixel(transform_points(lane_coords["xyz_right"][:, :2], world_to_image_space))
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore', np.RankWarning)
                    left_func = np.poly1d(np.polyfit(lane_coords["xyz_left"][:, 0], lane_coords["xyz_left"][:, 1], 2))
                    right_func = np.poly1d(np.polyfit(lane_coords["xyz_right"][:, 0], lane_coords["xyz_right"][:, 1], 2))
        
                    lane_indicies.append(idx)
                    left_funcs.append(left_func)
                    right_funcs.append(right_func)
                    
        selected_indicies=self.find_lanes_of_points(center_world,world_to_image_space,tl_faces,ego_translation,
                                                    img,raster_radius,active_tl_ids,lanes_lines,
                                 left_funcs,right_funcs,lane_indicies)
#         print(len(selected_indicies))
        self.find_adjacent_lanes(center_world,world_to_image_space, tl_faces,ego_translation,img,raster_radius,active_tl_ids,lanes_lines,
                                 left_funcs,right_funcs,lane_indicies,selected_indicies,ego_translation,ego_yaw)
        
        cv2.polylines(img, lanes_lines["default"], False, (255, 217, 82), lineType=cv2.LINE_AA, shift=CV2_SHIFT)
        cv2.polylines(img, lanes_lines["green"], False, (0, 255, 0), lineType=cv2.LINE_AA, shift=CV2_SHIFT)
        cv2.polylines(img, lanes_lines["yellow"], False, (255, 255, 0), lineType=cv2.LINE_AA, shift=CV2_SHIFT)
        cv2.polylines(img, lanes_lines["red"], False, (255, 0, 0), lineType=cv2.LINE_AA, shift=CV2_SHIFT)

        

        
#         # plot crosswalks
#         crosswalks = []
#         for idx in elements_within_bounds(center_world, self.bounds_info["crosswalks"]["bounds"], raster_radius):
#             crosswalk = self.proto_API.get_crosswalk_coords(self.bounds_info["crosswalks"]["ids"][idx])

#             xy_cross = cv2_subpixel(transform_points(crosswalk["xyz"][:, :2], world_to_image_space))
#             crosswalks.append(xy_cross)

#         cv2.polylines(img, crosswalks, True, (255, 117, 69), lineType=cv2.LINE_AA, shift=CV2_SHIFT)

        return (img ).astype(np.uint8)[::-1]

    def to_rgb(self, in_im: np.ndarray, **kwargs: dict) -> np.ndarray:
        return (in_im * 255).astype(np.uint8)[::-1]
