from typing import Dict
from tempfile import gettempdir
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from torchvision.models.resnet import resnet18
from tqdm import tqdm
import l5kit
from l5kit.configs import load_config_data
from l5kit.data import LocalDataManager, ChunkedDataset
from l5kit.dataset import AgentDataset, EgoDataset
from l5kit.rasterization import build_rasterizer,my_build_rasterizer
from l5kit.evaluation import write_pred_csv, compute_metrics_csv, read_gt_csv, create_chopped_dataset
from l5kit.evaluation.chop_dataset import MIN_FUTURE_STEPS
from l5kit.evaluation.metrics import neg_multi_log_likelihood, time_displace
from l5kit.geometry import transform_points,yaw_as_rotation33
from l5kit.visualization import PREDICTED_POINTS_COLOR, TARGET_POINTS_COLOR, draw_trajectory
from prettytable import PrettyTable
from pathlib import Path
import os





# set env variable for data
os.environ["L5KIT_DATA_FOLDER"] = "/home/mkhorasa/data_me/"
dm = LocalDataManager(None)
# get config
cfg = load_config_data("./agent_motion_config-history.yaml")
print(cfg)


# DIR_INPUT = "/kaggle/input/lyft-motion-prediction-autonomous-vehicles"
DIR_MODEL = "hostorymodel_HS"



device = torch.device("cuda:3" if torch.cuda.is_available() else "cpu")
print("device", device)
mean=torch.tensor([-2.703866066676344, -0.009803349945968614]).to(device, non_blocking=True)
var=torch.tensor([15.460930096936842, 0.032476811042323916]).to(device, non_blocking=True)




    
def build_scene_model():
    # load pre-trained Conv2D model
    model = resnet18(pretrained=True)

    # change input channels number to match the rasterizer's output
    num_in_channels = 3 
    model.conv1 = nn.Conv2d(
        num_in_channels,
        model.conv1.out_channels,
        kernel_size=model.conv1.kernel_size,
        stride=model.conv1.stride,
        padding=model.conv1.padding,
        bias=False,
    )
    # change output size to (X, Y) * number of future states
    num_targets = 200
    model.fc = nn.Linear(in_features=512, out_features=num_targets)

    return model
    
class HSLSTMClassifier(nn.Module):
 
    def __init__(self, input_dim, hidden_dim, middle_dim, target_dim):
        super(HSLSTMClassifier, self).__init__()
        self.res=build_scene_model()
        self.lstm =nn.LSTM(input_dim, hidden_dim,bidirectional=False )
        self.linear1 = nn.Linear(hidden_dim, middle_dim,bias=False)
        self.r1=nn.ReLU()
        self.linear2 = nn.Linear(256, 128,bias=True)
        self.linear3 = nn.Linear(128, 64,bias=True)
        
        self.linear4 = nn.Linear(64, target_dim,bias=True)
        self.linear5 = nn.Linear(64, target_dim,bias=True)
        
    def forward(self, x,x2):
        # print(x.shape)
        # x=torch.unsqueeze(torch.transpose(x,0,1),2)
        # print(x.shape)
        out2=self.res(x2)
        out,t = self.lstm(x)
        # print(out.shape)
        out = self.linear1(out[-1])
        out=self.r1(out)
        out3 = torch.cat((out, out2), 1)
        out3 = self.linear2(out3)
        out3=self.r1(out3)
        out3 = self.linear3(out3)
        out3=self.r1(out3)
        
        outx = self.linear4(out3)
        outy = self.linear5(out3)
        
        outx=torch.unsqueeze(outx,2)
        outy=torch.unsqueeze(outy,2)
        
        third_tensor = torch.cat((outx, outy), 2)
        # out=torch.softmax(out,dim=1)
        return third_tensor
    
    
def build_model(load=False) -> torch.nn.Module:
    model = HSLSTMClassifier(input_dim=2,hidden_dim=64,middle_dim=56,target_dim=50)
    return model


# for load from saved files
model = build_model().to(device, non_blocking=True)
optimizer = optim.Adam(model.parameters(), lr=1e-3)
criterion = nn.MSELoss(reduction="none")
model.load_state_dict(torch.load(DIR_MODEL,map_location='cuda:3'))




def forward_HSlstm(data, model, device, criterion):
    image = data["image"].to(device)
    inputs = data["history_positions"].to(device)
    inputs=(inputs-mean)/var
    inputs=torch.transpose(inputs,0,1).to(device)
    target_availabilities = data["target_availabilities"].unsqueeze(-1).to(device)
    targets=data["target_positions"].to(device)    
#     Forward pass
    outputs = model(inputs,image)
#     print(outputs.shape)
    outputs=outputs.reshape(targets.shape)
    loss = criterion(outputs, targets)
    # not all the output steps are valid, but we can filter them out from the loss using availabilities
    loss = loss * target_availabilities
    loss = loss.mean()
    return loss, outputs



## Dataset, dataloader

# ===== INIT DATASET
test_cfg = cfg["test_data_loader"]

# Rasterizer
rasterizer = my_build_rasterizer(cfg, dm)

# Test dataset/dataloader
test_zarr = ChunkedDataset(dm.require(test_cfg["key"])).open()
test_mask = np.load("/home/mkhorasa/data_me/mask.npz")["arr_0"]
test_dataset = AgentDataset(cfg, test_zarr, rasterizer, agents_mask=test_mask)
test_dataloader = DataLoader(test_dataset,
                             shuffle=test_cfg["shuffle"],
                             batch_size=test_cfg["batch_size"],
                             num_workers=test_cfg["num_workers"])


print(test_dataloader)


model.eval()

future_coords_offsets_pd = []
timestamps = []
agent_ids = []

with torch.no_grad():
    dataiter = tqdm(test_dataloader)
    
    for data in dataiter:
        image = data["image"].to(device)
        inputs = data["history_positions"]
        for i in range(inputs.shape[0]):
            inputs[i]=torch.tensor(transform_points(inputs[i], yaw_as_rotation33(-data["yaw"][i])))

        inputs=inputs.to(device, non_blocking=True)

        inputs=(inputs-mean)/var
        inputs=torch.transpose(inputs,0,1).to(device)


        target_availabilities = data["target_availabilities"].unsqueeze(-1).to(device, non_blocking=True)
        targets=data["target_positions"].to(device, non_blocking=True)
#     #     Forward pass
        outputs = model(inputs,image)
        outputs=outputs.reshape(targets.shape)
        outputs=outputs.cpu()
        for i in range(outputs.shape[0]):
            outputs[i]=torch.tensor(transform_points(outputs[i].cpu(), yaw_as_rotation33(data["yaw"][i])))

    # for CV end  
    
        future_coords_offsets_pd.append(outputs.numpy().copy())
        timestamps.append(data["timestamp"].numpy().copy())
        agent_ids.append(data["track_id"].numpy().copy())
    
    
    
pred_path = "submission.csv"

write_pred_csv(pred_path,
               timestamps=np.concatenate(timestamps),
               track_ids=np.concatenate(agent_ids),
               coords=np.concatenate(future_coords_offsets_pd),
              )